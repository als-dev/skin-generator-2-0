**Fonctionnement du Skin Generator**

Prérequis :
* Les images doivent êtres au préalable mise dans les **dossiers adéquat**. *(les jambes dans "3-Legs", le visage dans "1-Face", etc...)* 
* Les images doivent avoir la même résolution qui est de **64x64**
* Les textures doivent correspondre au template attribué pour chaque dossier *(Les templates se trouvent dans le fichier **Template_Example/...**)*
* Dans les dossiers, les images doivent être nommés de **0.png à x.png**
* Les images généré se retrouvent dans **RESULT/Gen N°x**


> Made by Daminator4113
> Helped by Kikipunk