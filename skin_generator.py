# -*-coding:Utf-8 -*
import os
import time


#############################################
###                SCRIPT                 ###
#############################################

from NEEDED.script import *

def LabelClear():
	label_success['text'] = ""

def generate():
	### Reset Texts ###
	label_error['text'] = ""
	label_successPath['text'] = ''
	label_successPath['state'] = 'disabled'

	CheckBoxs = (CheckVar5.get(), CheckVar6.get(), CheckVar7.get(), CheckVar8.get(), CheckVar9.get())
	try:
		SkinsNBR = int(entry_nbskin.get())
	except:
		label_success['text'] = "/!\\ Le nombre de skin est eroné"
		label_successPath['relief'] = 'flat'
		window.after(1500, LabelClear)
	else:
		if int(SkinsNBR) > 0 and isinstance(SkinsNBR, int):
			generate_skin(SkinsNBR, CheckBoxs, label_success, label_successPath, label_error, blank)
		else:
			label_success['text'] = "/!\\ Le nombre de skin est eroné"
			label_successPath['relief'] = 'flat'
			window.after(1500, LabelClear)



#############################################
###           CREATE INTERFACE            ###
#############################################

from tkinter import *

# Canvas
window = Tk()
window.title("ALS Skin Generator")
window.geometry("500x700")
window.iconbitmap("NEEDED/ALS.ico")
window.config(background='#7DBCD7')
window.resizable(False, False)
def blank(text):
	blank = Label(window, text=text, font=("Courier", 12), bg='#7DBCD7')
	blank.pack()
	return blank

# Nombre de skin à générer
blank("\n")
Label(window, text="Nombre de skin à générer", font=("Courier", 12), bg='#7DBCD7').pack()
entry_nbskin = Entry(window, width=10)
entry_nbskin.bind("<Return>", (lambda event: generate()))
entry_nbskin.pack()

# Dossiers à utiliser
blank("")
Label(window, text="Dossiers à utiliser", font=("Courier", 12), bg='#7DBCD7').pack()
CheckVar5 = IntVar()
CheckVar6 = IntVar()
CheckVar7 = IntVar()
CheckVar8 = IntVar()
CheckVar9 = IntVar()
checkbutton_5 = Checkbutton(window, text="5-Armor_Chest", width=10, bg='#7DBCD7', activebackground='#7DBCD7', variable = CheckVar5)
checkbutton_6 = Checkbutton(window, text="6-Armor_Legs", width=10, bg='#7DBCD7', activebackground='#7DBCD7', variable = CheckVar6)
checkbutton_7 = Checkbutton(window, text="7-Armor_Boots", width=10, bg='#7DBCD7', activebackground='#7DBCD7', variable = CheckVar7)
checkbutton_8 = Checkbutton(window, text="8-Armor_Head", width=10, bg='#7DBCD7', activebackground='#7DBCD7', variable = CheckVar8)
checkbutton_9 = Checkbutton(window, text="9-TopLayer", width=10, bg='#7DBCD7', activebackground='#7DBCD7', variable = CheckVar9)
checkbutton_5.pack()
checkbutton_6.pack()
checkbutton_7.pack()
checkbutton_8.pack()
checkbutton_9.pack()
checkbutton_5.select()
checkbutton_6.select()
checkbutton_7.select()
checkbutton_8.select()
checkbutton_9.select()

# Boutons
def Copy():
	window.clipboard_clear()
	window.clipboard_append(label_successPath['text'])
blank("\n")
Button(window, text="Générer!", width=10, command=generate).pack()
Button(window, text="Quit", width=10, command=exit).pack()

# Success Result
blank("\n")
label_success = Message(window, text="", font=("Courier", 12), bg='#7DBCD7', width=495)
label_successPath = Button(window, text="", font=("Courier", 9), bg='#7DBCD7', bd=2, wraplength=450, relief='groove', state='disabled', command=Copy)
label_error = Message(window, text="", font=("Courier", 8), bg='#7DBCD7', width=495)
label_success.pack()

window.mainloop()