# -*-coding:Utf-8 -*
import os
import time
path = os.getcwd()
#os.chdir("C:/Users/%USERPROFILE%/Desktop/SkinsGenerator")


def generate_skin(SkinsNBR,CheckBoxs,label_success,label_successPath,label_error,blank):
	
	size = (64,64)

	#############################################
	### CREATE NEW FOLDER FOR EACH GENERATION ###
	#############################################

	GetError1 = []
	GetError2 = []
	def Error(ErrorType):
		if ErrorType == 0:
			label_error['text'] = label_error['text'] + "/!\\ Il n'y a pas de fichiers dans " + SkinParts[k] + "\n"
		elif ErrorType == 1 :
			if k not in GetError1:
				GetError1.append(k)
				label_error['text'] = label_error['text'] + "/!\\ Le fichier " + str(r) + ".png n'a pas été trouvé dans le dossier " + SkinParts[k] + "\n"
		elif ErrorType == 2 :
			if k not in GetError2:
				GetError2.append(k)
				label_error['text'] = label_error['text'] + "/!\\ Le fichier " + str(r) + ".png, du dossier " + SkinParts[k] + " n'est pas conforme !\n"

	def ResultFolder():
		PathResult = os.listdir(path+"/RESULT")
		LastNbr = 1

		while 1:
			try:
				os.mkdir(path+"/RESULT/Gen N°"+str(LastNbr))
				PathResult = path+"/RESULT/Gen N°"+str(LastNbr)
			except:
				LastNbr += 1
				continue
			else:
				break
		return PathResult

	############################################
	###				GENERATE SKINS			 ###
	############################################

	from PIL import Image
	from random import randint
	
	#b = input("Number of skins desired : ")
	b = SkinsNBR
	b = int(b)

	PathResult = ResultFolder()
	SkinParts = os.listdir(path)

	for i in range(0,b):
		#label_success['text'] = "Work in progress "+str(i+1)+"/"+str(b)
		#print("Work in progress "+str(i+1)+"/"+str(b))
		new_skin = Image.new("RGBA", size, color=(0,0,0,0))

		### Skin Base : 0 1 2 3 4 ###
		for k in range(0,5): #k = Skin Parts
			FilesNbr = len(os.listdir(path+"/"+SkinParts[k]))-1 # ----------------- #Max Number of Variant files

			if FilesNbr == -1 and i == 0: # --------------------------------------- #On Check s'il y a des fichiers dans les "Skin Parts"
				Error(0)

			if FilesNbr != -1:
				r = randint(0,FilesNbr) # ----------------------------------------- #r = Random Parts
				CalquePath = path+"/"+SkinParts[k]+"/"
				try:
					calque = Image.open(CalquePath+str(r)+".png")
				except:
					Error(1)
				else:
					calque = calque.convert("RGBA")
					try:
						new_skin = Image.alpha_composite(new_skin, calque)
					except:
						Error(2)

		### Skin Layers : 5 6 7 8 9 ###
		for k in range(5,10): # --------------------------------------------------- #k = Skin Parts
			Kstring=str(k)
			if CheckBoxs[k-5] == 1: # --------------------------------------------- #Get Checkbox
				FilesNbr = len(os.listdir(path+"/"+SkinParts[k]))-1 # ------------- #Max Number of Variant files

				if FilesNbr == -1 and i == 0:
					Error(0)

				r = randint(-1,FilesNbr) #r = Random Parts

				if r != -1: #-1 = Nothing to add (only for layers)
					CalquePath = path+"/"+SkinParts[k]+"/"
					try:
						calque = Image.open(CalquePath+str(r)+".png")
					except:
						Error(1)
					else:
						calque = calque.convert("RGBA")
						try:
							new_skin = Image.alpha_composite(new_skin, calque)
						except:
							Error(2)

		### Export Result ###
		NameResult = PathResult+"/Result_"+str(i)+".png" # ----------------------- #(Destination, "FileName + Gen Order", "File Type")
		new_skin.save(NameResult, "PNG")

	### Print Result and Debug ###
	label_success['text'] = "New Skins are successfully created in"
	label_successPath.pack()
	label_successPath['state'] = 'normal'
	label_successPath['relief'] = 'groove'
	label_successPath['text'] = PathResult
	blank("")
	label_error.pack()
